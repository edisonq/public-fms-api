/**
 * Expenses.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    expenseName: {
      type: 'string',
      required: true
    },
    expenseDescription: {
      type: 'string'
    },
    expenseDate: {
      type: 'string', columnType: 'datetime',
      required: true
    },
    distanceTravelled: {
      type: 'number',
      allowNull: true
    },
    cost:  {
      type: 'number'
    },
    extendedCost: {
      type: 'number',
      allowNull: true
    },
    notes: {
      type: 'string',
      allowNull: true
    },
    isDeleted: {
      type: 'boolean',
      defaultsTo: false
    },
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    vehicle: {
      model: 'vehicles'
    },
    partsUsed: {
      model: 'parts'
    }
  },

};

