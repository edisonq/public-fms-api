/**
 * Remittance.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
    amountPaid: {
      type: 'number'
    },
    amountDue: {
      type: 'number'
    },
    type: {
      // 0= not in the list, 1= fuel, 2= rental, 3= rent-to-own 4= downpayment
      type: 'string',
      isIn: ['not in the list', 'fuel', 'rental', 'rent to own', 'downpayment'],
      defaultsTo: 'on-garage'
    },
    datePayment: {
      type: 'string', columnType: 'datetime'
    },
    fullPayment: {
      type: 'boolean'
    },
    remarks: {
      type: 'string'
    },
    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    rentToOwnList: {
      collection: 'rentToOwnList',
      via: 'remittance'
    },
    due: {
      model: 'due'
    },
    amountReceivedBy:  {
      model: 'user'
    }
  },

};

