/**
 * Parts.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
    manufacturerserialnumber: {
      type: 'string',
      required: true
    },
    inhouseserial: {
      type: 'string',
      required: true
    },
    vendor: {
      type: 'string'
    },
    limitmileage: {
      type: 'number'
    },
    limitdays: {
      type: 'number'
    },
    unitCost: {
      type: 'number'
    },
    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    partsSchedule: {
      collection: 'partsSchedule',
      via: 'parts'
    },
    partsVehicle: {
      collection: 'partsVehicle',
      via: 'part'
    },
    warehouseParts: {
      collection: 'warehouseParts',
      via: 'part'
    },
    dueParts: {
      collection: 'dueParts',
      via: 'part'
    },
    expense: {
      collection: 'expenses',
      via: 'partsUsed'
    },
    expenseAutomated: {
      collection: 'expenseAutomated',
      via: 'part'
    },
    partsInventory: {
      model: 'partsInventory'
    },
    disposal: {
      model: 'disposal'
    },
    // vehicle: {
    //   model: 'vehicles'
    // },
    // repairVehicle: {
    //   model: 'repairVehicle'
    // },
    warranty: {
      model: 'warranty'
    }
  },
};

