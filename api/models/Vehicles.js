/**
 * Vehicles.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
    bodyNumber: {
      type: 'string',
      required: true
    },
    LTRFBnumber: {
      type: 'string'
    },
    plateNumber: {
      type: 'string',
      required: true
    },
    bodySerialNumber: {
      type: 'string'
    },
    engineSerialNumber: {
      type: 'string'
    },
    status: {
      type: 'string',
      isIn: ['on-garage', 'out', 'repair or maintenance', 'disposed'],
      defaultsTo: 'on-garage'
    },
    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    garageVehicle: {
      collection: 'garageVehicle',
      via: 'vehicle'
    },
    meterReading:  {
      collection: 'meterReading',
      via: 'vehicle'
    },
    inspection: {
      collection: 'inspection',
      via: 'vehicle'
    },
    expenseAutomated: {
      collection: 'expenseAutomated',
      via: 'vehicle'
    },
    expenses:  {
      collection: 'expenses',
      via: 'vehicle'
    },
    fuelTransaction: {
      collection: 'fuelTransaction',
      via: 'vehicle'
    },
    peopleVehicle: {
      collection: 'peopleVehicle',
      via: 'vehicle'
    },
    pmSchedule: {
      collection: 'pmSchedule',
      via: 'vehicle'
    },
    rentToOwn: {
      collection: 'rentToOwn',
      via: 'vehicle'
    },
    rentalRate: {
      collection: 'rentalRate',
      via: 'vehicle'
    },
    partsVehicle: {
      collection: 'partsVehicle',
      via: 'vehicle'
    },
    repairVehicle: {
      collection: 'repairVehicle',
      via: 'vehicle'
    },
    disposal: {
      model: 'disposal'
    },
    makerModel: {
      model: 'makerModel'
    },
    warranty: {
      model: 'warranty'
    }
  },

};

