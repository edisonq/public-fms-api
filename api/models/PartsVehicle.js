/**
 * PartsVehicle.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
    dateAttached: {
      type: 'string', columnType: 'datetime'
    },
    dateDetached: {
      type: 'string', columnType: 'datetime'
    },
    dateEstimatedExpiration: {
      type: 'string', columnType: 'datetime'
    },
    estimatedDistanceLimit: {
      type: 'number'
    },
    attached: {
      type: 'boolean'
    },
    currentDistanceAttached: {
      type: 'number'
    },
    currentDistanceDetached: {
      type: 'number'
    },

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    part: {
      model: 'parts'
    },
    repairVehicle: {
      model: 'repairVehicle'
    },
    vehicle: {
      model: 'vehicles'
    }
  },

};

