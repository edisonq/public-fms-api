/**
 * User.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝
    email: {
      type: 'string',
      required: true,
      unique: true
    },
    password: {
      type: 'string',
      required: true
    },
    fullName: {
      type: 'string',
      required: true
    },
    isSuperAdmin: {
      type: 'boolean',
      defaultsTo: false
    },
    passwordResetToken: {
      type: 'string'
    },
    passwordResetTokenExpiresAt: {
      type: 'string',
      columnType: 'datetime'
    },
    emailProofToken: {
      type: 'string'
    },
    emailProofTokenExpiresAt: {
      type: 'string',
      columnType: 'datetime'
    },
    emailStatus: {
      type: 'string'
    },
    emailChangeCandidate:{
      type: 'boolean',
      defaultsTo: false
    },

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    remittance: {
      collection: 'remittance',
      via: 'amountReceivedBy'
    },
    due: {
      collection: 'due',
      via: 'logBy'
    },
    fuelTransaction: {
      collection: 'fuelTransaction',
      via: 'logBy'
    },
    fuelRefill: {
      collection: 'fuelRefill',
      via: 'logBy'
    },
    disposal: {
      collection: 'disposal',
      via: 'logBy'
    },
    companyUser: {
      collection: 'companyUser',
      via: 'user'
    },
    rentToOwn: {
      collection: 'rentToOwn',
      via: 'logBy'
    }
  },

};

