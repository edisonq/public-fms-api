/**
 * People.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    firstname: {
      type: 'string'
    },
    middlename: {
      type: 'string'
    },
    lastname: {
      type: 'string'
    },
    type: {
      type: 'string',
      isIn: ['driver', 'mechanic', 'accountant', 'custodian', 'administrator', 'micab-CS'],
      defaultsTo: 'on-garage'
    },
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    companyPeople: {
      collection: 'companyPeople',
      via: 'people'
    },
    peopleVehicle: {
      collection: 'peopleVehicle',
      via: 'driver'
    },
    rentToOwn: {
      collection: 'rentToOwn',
      via: 'driver'
    },
    rentalRate: {
      collection: 'rentalRate',
      via: 'driver'
    },
    repairVehicle: {
      collection: 'repairVehicle',
      via: 'mechanic'
    }
  },

};

